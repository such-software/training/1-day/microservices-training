const createConfig = require('../lib/config')
const env = require('../lib/env')

const config = createConfig({ env })

module.exports = { config }
