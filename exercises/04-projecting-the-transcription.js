const {
  project
} = require('@suchsoftware/proof-of-concept-message-store/lib/read')
const TranscribedControls = require('../lib/transcribe-component/controls/events/transcribed')
const Transcribe = require('../lib/transcribe-component/transcribe')

// TODO: Fill out the projection required here
const projection = require('../lib/transcribe-component/projection')

const events = [TranscribedControls.example()]

const transcription = new Transcribe()
project(events, projection, transcription)

console.log({ transcription })
console.log(
  'Has the file been transcribed?',
  transcription.transcribed() ? 'Yes' : 'No'
)
