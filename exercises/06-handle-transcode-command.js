const { config } = require('./prelude')
const TranscodeControls = require('../lib/transcode-component/controls/commands/transcode')

const transcode = TranscodeControls.example()

// TODOs for this exercise:
// - The TODOs in src/transcode-component/index.js - Handle this command
// - The TODO in src/transcode-component/projection.js - Write the projection

config.transcodeComponent.commandHandlers
  .Transcode(transcode)
  // Notice the double handling again
  .then(() => config.transcodeComponent.commandHandlers.Transcode(transcode))
  .then(() => console.log('Transcoded.'))
  .finally(config.messageStore.stop)
