// config.js is the heart of the dependency injection we use.  It is in this
// file that we piece together the actual runtime values.  This file breathes
// the breath of life into the otherwise hollow shell of the rest of the
// system.

const MessageStore = require('@suchsoftware/proof-of-concept-message-store')

const HomeApplication = require('./home-application')
const PostgresClient = require('./postgres-client')
const KnexClient = require('./knex-client')
const TranscribeComponent = require('./transcribe-component')
const TranscodeComponent = require('./transcode-component')
const CatalogComponent = require('./catalog-component')
const VideoListAggregator = require('./video-list-aggregator')

// Even the configuration has a dependency, namely the run-time environment.
function createConfig ({ env }) {
  // We build a Postgres client connection
  const postgresClient = PostgresClient({
    connectionString: env.messageStoreConnectionString
  })
  // The message store code receives that client connection.  This way, if we
  // want to do something else with that same connection, we can.  It's Just
  // Postgres™.
  const messageStore = MessageStore({ session: postgresClient })

  const knexClient = KnexClient({
    connectionString: env.databaseUrl
  })

  // Applications
  const homeApplication = HomeApplication({ db: knexClient, messageStore })

  // Components
  const transcribeComponent = TranscribeComponent({ messageStore })
  const transcodeComponent = TranscodeComponent({ messageStore })
  const catalogComponent = CatalogComponent({ messageStore })

  // Aggregators
  const videoListAggregator = VideoListAggregator({
    db: knexClient,
    messageStore
  })

  const consumers = [
    transcribeComponent,
    transcodeComponent,
    catalogComponent,
    videoListAggregator
  ]

  return {
    consumers,
    env,
    homeApplication,
    messageStore,
    transcribeComponent,
    transcodeComponent,
    catalogComponent,
    videoListAggregator
  }
}

module.exports = createConfig
