module.exports = {
  Transcoded (transcoding, transcoded) {
    transcoding.id = transcoded.data.transcodeId
    transcoding.url = transcoded.data.url
    transcoding.transcodedUrl = transcoded.data.transcodedUrl
  }
}
