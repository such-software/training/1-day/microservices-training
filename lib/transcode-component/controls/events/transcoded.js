const IdControls = require('../id')

module.exports = {
  example () {
    const transcodeId = IdControls.example()

    return {
      id: IdControls.example(),
      type: 'Transcoded',
      metadata: {},
      data: {
        transcodeId,
        url: this.url(),
        transcodedUrl: this.transcodedUrl()
      }
    }
  },

  url () {
    return 'https://www.youtube.com/watch?v=GI_P3UtZXAA'
  },

  transcodedUrl () {
    return 'https://www.youtube.com/watch?v=dQw4w9WgXcQ'
  }
}
