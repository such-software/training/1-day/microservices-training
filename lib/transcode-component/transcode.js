class Transcode {
  id
  url
  transcodedUrl

  transcoded () {
    return !!this.transcodedUrl
  }
}

module.exports = Transcode
