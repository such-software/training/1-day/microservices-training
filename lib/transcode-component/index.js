const uuid = require('uuid/').v4

const projection = require('./projection')
const Transcode = require('./transcode')

// Returns a url to the transcoded file
function transcodeFile (source) {
  // More simulation shenanigans
  console.log('If real transcoding were going on, you bet it would be here')

  return 'https://www.youtube.com/watch?v=dQw4w9WgXcQ'
}

// Fleshing out these handlers is the main activity of the workshop.
function CommandHandlers ({ messageStore }) {
  return {
    async Transcode (transcode) {
      const transcodeId = transcode.data.transcodeId
      const transcodeJob = new Transcode()
      const streamName = `transcode-${transcodeId}`

      await messageStore.fetch(streamName, projection, transcodeJob)

      if (transcodeJob.transcoded()) {
        console.log(`(${transcode.id}): Already transcoded. Skipping.`)

        return true
      }

      const transcodedUrl = transcodeFile(transcode.data.url)

      const transcoded = {
        id: uuid(),
        type: 'Transcoded',
        metadata: {
          traceId: transcode.metadata.traceId,
          originStreamName: transcode.metadata.originStreamName
        },
        data: {
          videoId: transcode.data.videoId,
          url: transcode.data.url,
          transcodedUrl
        }
      }

      return messageStore.write(streamName, transcoded)
    }
  }
}

function createComponent ({ messageStore }) {
  const commandHandlers = CommandHandlers({ messageStore })

  const commandSubscription = messageStore.createSubscription({
    streamName: 'transcode:command',
    handlers: commandHandlers,
    subscriberId: 'transcodeCommandConsumer'
  })

  function start () {
    console.log('Starting transcode component')

    commandSubscription.start()
  }

  return {
    commandHandlers,
    start
  }
}

module.exports = createComponent
