module.exports = {
  Started (video, started) {
    video.id = started.data.videoId
  },

  Transcribed (video, transcribed) {
    video.transcription = transcribed.data.transcription
  },

  Transcoded (video, transcoded) {
    video.url = transcoded.data.transcodedUrl
  }
}
