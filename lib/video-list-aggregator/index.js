const projection = require('./projection')
const Video = require('./video')

function createHandlers ({ messageStore, queries }) {
  return {
    async Cataloged (cataloged) {
      const video = new Video()

      await messageStore.fetch(cataloged.streamName, projection, video)

      return queries.upsertVideo(video)
    }
  }
}

function createQueries ({ db }) {
  return {
    upsertVideo (video) {
      const rawQuery = `
        INSERT INTO
          videos (id, url, transcription)
        VALUES (:id, :url, :transcription)
        ON CONFLICT (id) DO NOTHING
      `
      const attributes = {
        id: video.id,
        url: video.url,
        transcription: video.transcription
      }

      return db.then(client => client.raw(rawQuery, attributes))
    }
  }
}

function createComponent ({ db, messageStore }) {
  const queries = createQueries({ db })
  const handlers = createHandlers({ messageStore, queries })

  const subscription = messageStore.createSubscription({
    streamName: 'catalog',
    handlers,
    subscriberId: 'videoListAggregator'
  })

  function start () {
    console.log('Starting video list aggregator')

    subscription.start()
  }

  return {
    start
  }
}

module.exports = createComponent
