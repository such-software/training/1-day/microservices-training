const IdControls = require('../id')

module.exports = {
  example () {
    return {
      id: IdControls.example(),
      type: 'Transcoded',
      metadata: {},
      data: {
        videoId: IdControls.example(),
        url: this.url(),
        transcodeId: IdControls.example(),
        transcodedUrl: this.transcodedUrl()
      }
    }
  },

  url () {
    return 'https://www.youtube.com/watch?v=GI_P3UtZXAA'
  },

  transcodedUrl () {
    return 'https://www.youtube.com/watch?v=dQw4w9WgXcQ'
  }
}
