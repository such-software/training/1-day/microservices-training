const IdControls = require('../id')

module.exports = {
  example () {
    return {
      id: IdControls.example(),
      type: 'Catalog',
      metadata: {},
      data: {
        videoId: IdControls.example(),
        url: this.url(),
        transcodeId: IdControls.example(),
        transcribeId: IdControls.example()
      }
    }
  },

  url () {
    return 'https://www.youtube.com/watch?v=GI_P3UtZXAA'
  }
}
