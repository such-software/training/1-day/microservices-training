class Catalog {
  id
  url
  transcodeId
  transcodedUrl
  transcribeId
  transcription
  catalogCompleted

  started () {
    return !!this.url
  }

  transcoded () {
    return !!this.transcodedUrl
  }

  transcribed () {
    return !!this.transcription
  }

  cataloged () {
    return !!this.catalogCompleted
  }
}

module.exports = Catalog
