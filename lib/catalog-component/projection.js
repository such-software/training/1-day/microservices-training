module.exports = {
  Started (video, started) {
    video.id = started.data.videoId
    video.url = started.data.url
    video.transcribeId = started.data.transcribeId
    video.transcodeId = started.data.transcodeId
  },

  Transcribed (video, transcribed) {
    video.transcription = transcribed.data.transcription
  },

  Transcoded (video, transcoded) {
    video.transcodedUrl = transcoded.data.transcodedUrl
  },

  Cataloged (video, cataloged) {
    video.catalogCompleted = true
  }
}
