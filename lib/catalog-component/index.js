const uuid = require('uuid').v4

const projection = require('./projection')
const Catalog = require('./catalog')

// This component has 4 sets of handlers:
// 1. Its command stream
// 2. Its event stream
// 3. transcode's event stream
// 4. transcribe's event stream
function CommandHandlers ({ messageStore }) {
  return {
    async Catalog (catalog) {
      const videoId = catalog.data.videoId
      const streamName = `catalog-${videoId}`
      const video = new Catalog()

      await messageStore.fetch(streamName, projection, video)

      if (video.started()) {
        console.log(`(${catalog.id}) Catalog already started. Skipping`)

        return true
      }

      const started = {
        id: uuid(),
        type: 'Started',
        metadata: {
          traceId: catalog.metadata.traceId
        },
        data: {
          videoId: catalog.data.videoId,
          url: catalog.data.url,
          transcodeId: uuid(),
          transcribeId: uuid()
        }
      }

      return messageStore.write(streamName, started)
    }
  }
}

function EventHandlers ({ messageStore }) {
  return {
    async Started (started) {
      const videoId = started.data.videoId
      const catalogStreamName = `catalog-${videoId}`
      const video = new Catalog()

      await messageStore.fetch(catalogStreamName, projection, video)

      if (video.transcoded()) {
        console.log(`(${started.id}) Video already transcoded. Skipping`)

        return true
      }

      const transcodeId = started.data.transcodeId

      const transcode = {
        id: uuid(),
        type: 'Transcode',
        metadata: {
          traceId: started.metadata.traceId,
          originStreamName: catalogStreamName
        },
        data: {
          transcodeId,
          url: video.url
        }
      }
      const transcodeStreamName = `transcode:command-${transcodeId}`

      return messageStore.write(transcodeStreamName, transcode)
    },

    async Transcoded (transcoded) {
      const videoId = transcoded.data.videoId
      const catalogStreamName = `catalog-${videoId}`
      const video = new Catalog()

      await messageStore.fetch(catalogStreamName, projection, video)

      if (video.transcribed()) {
        console.log(`(${transcoded.id}) Video already transcribed. Skipping`)

        return true
      }

      const transcribeId = video.transcribeId

      const transcribe = {
        id: uuid(),
        type: 'Transcribe',
        metadata: {
          traceId: transcoded.metadata.traceId,
          originStreamName: catalogStreamName
        },
        data: {
          transcribeId,
          url: video.url
        }
      }
      const transcribeStreamName = `transcribe:command-${transcribeId}`

      return messageStore.write(transcribeStreamName, transcribe)
    },

    async Transcribed (transcribed) {
      const videoId = transcribed.data.videoId
      const catalogStreamName = `catalog-${videoId}`
      const video = new Catalog()

      await messageStore.fetch(catalogStreamName, projection, video)

      if (video.cataloged()) {
        console.log(`(${transcribed.id}) Video already cataloged. Skipping`)

        return true
      }

      const cataloged = {
        id: uuid(),
        type: 'Cataloged',
        metadata: {
          traceId: transcribed.metadata.traceId
        },
        data: {
          videoId
        }
      }

      return messageStore.write(catalogStreamName, cataloged)
    }
  }
}

function TranscodeEventHandlers ({ messageStore }) {
  return {
    async Transcoded (transcoded) {
      const catalogStreamName = transcoded.metadata.originStreamName
      const [_, videoId] = catalogStreamName.split(/-(.+)/)
      const video = new Catalog()

      await messageStore.fetch(catalogStreamName, projection, video)

      if (video.transcoded()) {
        console.log(`(${transcoded.id}) Already transcoded. Skipping.`)

        return true
      }

      const catalogTranscoded = {
        id: uuid(),
        type: 'Transcoded',
        metadata: {
          traceId: transcoded.metadata.traceId
        },
        data: {
          videoId: videoId,
          transcodeId: transcoded.data.transcodeId,
          url: transcoded.data.url,
          transcodedUrl: transcoded.data.transcodedUrl
        }
      }

      return messageStore.write(catalogStreamName, catalogTranscoded)
    }
  }
}

function TranscribeEventHandlers ({ messageStore }) {
  return {
    async Transcribed (transcribed) {
      const catalogStreamName = transcribed.metadata.originStreamName
      const [_, videoId] = catalogStreamName.split(/-(.+)/)
      const video = new Catalog()

      await messageStore.fetch(catalogStreamName, projection, video)

      if (video.transcribed()) {
        console.log(`(${transcribed.id}) Already transcribed. Skipping.`)

        return true
      }

      const catalogTranscribed = {
        id: uuid(),
        type: 'Transcribed',
        metadata: {
          traceId: transcribed.metadata.traceId
        },
        data: {
          videoId: videoId,
          transcribeId: transcribed.data.transcribeId,
          url: transcribed.data.url,
          transcription: transcribed.data.transcription
        }
      }

      return messageStore.write(catalogStreamName, catalogTranscribed)
    }
  }
}

function createComponent ({ messageStore }) {
  const commandHandlers = CommandHandlers({ messageStore })
  const eventHandlers = EventHandlers({ messageStore })
  const transcodeEventHandlers = TranscodeEventHandlers({ messageStore })
  const transcribeEventHandlers = TranscribeEventHandlers({
    messageStore
  })

  const commandSubscription = messageStore.createSubscription({
    streamName: 'catalog:command',
    handlers: commandHandlers,
    subscriberId: 'catalogCommandConsumer'
  })

  const eventSubscription = messageStore.createSubscription({
    streamName: 'catalog',
    handlers: eventHandlers,
    subscriberId: 'catalogEventConsumer'
  })

  const transcodeEventSubscription = messageStore.createSubscription({
    streamName: 'transcode',
    handlers: transcodeEventHandlers,
    originStreamName: 'catalog',
    subscriberId: 'catalogTranscodeEventConsumer'
  })

  const transcribeEventSubscription = messageStore.createSubscription({
    streamName: 'transcribe',
    handlers: transcribeEventHandlers,
    originStreamName: 'catalog',
    subscriberId: 'catalogTranscribeEventConsumer'
  })

  function start () {
    console.log('Starting video catalog component')

    commandSubscription.start()
    eventSubscription.start()
    transcodeEventSubscription.start()
    transcribeEventSubscription.start()
  }

  return {
    commandHandlers,
    eventHandlers,
    start,
    transcodeEventHandlers,
    transcribeEventHandlers
  }
}

module.exports = createComponent
