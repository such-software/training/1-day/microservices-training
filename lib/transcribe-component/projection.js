module.exports = {
  Transcribed (transcribe, transcribed) {
    transcribe.id = transcribed.data.transcribeId
    transcribe.url = transcribed.data.url
    transcribe.transcription = transcribed.data.transcription
  }
}
