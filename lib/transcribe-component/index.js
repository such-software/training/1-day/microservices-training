const uuid = require('uuid').v4

const projection = require('./projection')
const Transcribe = require('./transcribe')

// Function for faking the transcription
function transcribeVideo (uri) {
  return `
    We're no strangers to love
    You know the rules and so do I...
  `
}

// Handlers will do the actual work of handling messages.
// They are analogous to HTTP handlers, only the stimulus they respond to is
// one of the messages communicated over pub/sub.
//
// Fleshing out these handlers is the main activity of the workshop.
function CommandHandlers ({ messageStore }) {
  return {
    async Transcribe (transcribe) {
      const { transcribeId, url } = transcribe.data
      const transcription = transcribeVideo(url)
      const transcribeJob = new Transcribe()

      await messageStore.fetch(
        `transcribe-${transcribeId}`,
        projection,
        transcribeJob
      )

      if (transcribeJob.transcribed()) {
        console.log(`[${transcribe.id}]: Already transcribed. Skipping.`)

        return true
      }

      const transcribed = {
        id: uuid(),
        type: 'Transcribed',
        metadata: {
          traceId: transcribe.metadata.traceId,
          originStreamName: transcribe.metadata.originStreamName
        },
        data: {
          transcribeId,
          url,
          transcription
        }
      }
      const streamName = `transcribe-${transcribeId}`

      return messageStore.write(streamName, transcribed)
    }
  }
}

// This top-level function will receive dependencies in future steps
function build ({ messageStore }) {
  const commandHandlers = CommandHandlers({ messageStore })

  const commandSubscription = messageStore.createSubscription({
    streamName: 'transcribe:command',
    handlers: commandHandlers,
    subscriberId: 'transcribeCommandConsumer'
  })
  // Components get new messages to process by polling the message store.
  // We decouple actually starting the component from the rest of its
  // definition.  Naturally, starting the polling cycle in test would proveo
  // problematic.
  //
  // The convention in this code base is that each component exposes a `start`
  // function that gets picked up in `src/index.js`.
  function start () {
    console.log('Starting transcribe component')

    commandSubscription.start()
  }

  return {
    commandHandlers,
    start
  }
}

module.exports = build
