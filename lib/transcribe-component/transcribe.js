class Transcribe {
  id
  url
  transcription

  transcribed () {
    return !!this.transcription
  }
}

module.exports = Transcribe
