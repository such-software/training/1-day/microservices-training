const IdControls = require('../id')

module.exports = {
  example () {
    const transcribeId = IdControls.example()

    return {
      id: IdControls.example(),
      type: 'Transcribed',
      metadata: {},
      data: {
        transcribeId,
        url: this.url(),
        transcription: this.transcription()
      }
    }
  },

  transcription () {
    return "We're no strangers to love..."
  },

  url () {
    return 'https://www.youtube.com/watch?v=dQw4w9WgXcQ'
  }
}
