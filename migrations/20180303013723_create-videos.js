exports.up = knex =>
  knex.schema.createTable('videos', table => {
    table.string('id').primary()
    table.string('url')
    table.text('transcription')
  })

exports.down = knex => knex.schema.dropTable('videos')
